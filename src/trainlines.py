'''
 Copyright (C) 2023  Amy King

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 3.

 londontransit is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
import tfl, json

def getTrainLines():
	# Simulate train line data as a list of dictionaries
	train_lines = [
			{"name": "Bakerloo", "color": "#B26300", "id":"bakerloo"},
			{"name": "Cable Car", "color": "#734FA0", "id": "london-cable-car"},
			{"name": "Central", "color": "#DC241F", "id": "central"},
			{"name": "Circle", "color": "#FFCD00", "id": "circle"},
			{"name": "District", "color": "#007D32", "id": "district"},
			{"name": "DLR", "color": "#00A4A7", "id": "dlr"},
			{"name": "Elizabeth line", "color": "#6950A1", "id": "elizabeth"},
			{"name": "Hammersmith & City", "color": "#F589A6", "id": "hammersmith-city"},
			{"name": "Liberty", "color": "#5D6061", "id": "liberty"},
			{"name": "Lioness", "color": "#FAA81A", "id": "lioness"},
			{"name": "Metropolitan", "color": "#9B0058", "id": "metropolitan"},
			{"name": "Mildmay", "color": "#0076AD", "id": "mildmay"},
			{"name": "Northern", "color": "#000000", "id": "northern"},
			{"name": "Piccadilly", "color": "#0019A8", "id": "piccadilly"},
			{"name": "Jubilee", "color": "#838D93", "id": "jubilee"},
			{"name": "Suffragette", "color": "#5BBD72", "id": "suffragette"},
			{"name": "Thameslink", "color": "#FF5AA4", "id": "thameslink"},
			{"name": "Trams", "color": "#84B817", "id": "tram"},
			{"name": "Victoria", "color": "#039BE5", "id": "victoria"},
			{"name": "Waterloo & City", "color": "#76D0BD", "id": "waterloo-city"},
			{"name": "Weaver", "color": "#823A62", "id": "weaver"},
			{"name": "Windrush", "color": "#ED1C00", "id": "windrush"}
	]

	for i in train_lines:
		print(i["name"])
		status = tfl.get_tube_status(i["id"])
		i["status"] = status[0]
		i["reason"] = status[1]

	# Convert the list of dictionaries to a JSON string
	return json.dumps(train_lines)
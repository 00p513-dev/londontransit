import requests

def get_tube_status(line_id):
	"""Get the status of a specific tube line."""
	api_url = f"https://api.tfl.gov.uk/Line/{line_id}/Status"
	response = requests.get(api_url)

	if response.status_code == 200:
		status = response.json()[0]['lineStatuses'][0]['statusSeverityDescription']

		try:
			reason = response.json()[0]['lineStatuses'][0]['reason']
		except:
			reason = status

		return status, reason
	else:
		print(f"Failed to fetch the status for line {line_id}. Status code: {response.status_code}")
		return ("Error", "Error")
/*
 * Copyright (C) 2023 Amy King
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * londontransit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import io.thp.pyotherside 1.4

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'londontransit.00p513-dev'
    automaticOrientation: true

    width: units.gu(45)
    height: units.gu(75)

    // Python integration
    Python {
        id: python

        Component.onCompleted: {
            addImportPath(Qt.resolvedUrl('../src/'));

            // Import Python module to fetch train line data
            importModule('trainlines', function() {
                console.log('trainlines module imported');
                python.call('trainlines.getTrainLines', [], function(returnValue) {
                    // Parse the returned JSON data
                    var trainLinesData = JSON.parse(returnValue);

                    // Update the model with the fetched data
                    trainLinesModel.clear();
                    for (var i = 0; i < trainLinesData.length; i++) {
                        trainLinesModel.append({name: trainLinesData[i].name, color: trainLinesData[i].color, status: trainLinesData[i].status, reason: trainLinesData[i].reason});
                    }
                });
            });
        }

        onError: {
            console.log('python error: ' + traceback);
        }


    }

    // Train Lines List Model
    ListModel {
        id: trainLinesModel
        // Initial sample data
        ListElement { name: "Loading..."; color: "gray"; status: "Fetching data"; reason: "Still fetching data..." }
    }

    Page {
        anchors.fill: parent

        header: PageHeader {
            id: header
            title: i18n.tr('Tube Status')
        }

        // Train Lines ListView
        ListView {
            anchors {
                top: header.bottom
                left: parent.left
                right: parent.right
                bottom: parent.bottom
            }
            anchors.margins: units.gu(1)
            model: trainLinesModel
            spacing: units.gu(1)

            delegate: ListItem {
                width: parent.width
                height: units.gu(8)

                MouseArea {
                    width: parent.width
                    height: parent.height

                    onClicked: {
                        // Handle button click, you can provide more information here
                        console.log("Clicked on " + model.name + " line. Status: " + model.status);
                        // lineDialogComponent.text = model.status;
                        // lineDialogComponent.title = model.name;
                        PopupUtils.open(lineDialogComponent, root, {title: model.name, text: model.reason})
                    }

                    Rectangle {
                        width: parent.width
                        height: parent.height
                        color: model.color
                        radius: units.gu(1)
                    }

                    Label {
                        anchors {
                            verticalCenter: parent.verticalCenter
                            left: parent.left
                            leftMargin: units.gu(1)
                        }
                        text: model.name
                        font.bold: true
                        color: "white"
                    }

                    Label {
                        anchors {
                            verticalCenter: parent.verticalCenter
                            right: parent.right
                            rightMargin: units.gu(1)
                        }
                        text: model.status
                        color: "white"
                    }
                }
            }
        }
        
        // Define Line Status dialog
        Component {
            id: lineDialogComponent
            Dialog {
                id: lineDialog
                title: ""
                text: ""

                Button {
                    text: "OK"
                    onClicked: PopupUtils.close(lineDialog)
                }
            }
        }
    }
}
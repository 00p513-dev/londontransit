# London Transit

A simple app for using public transit in London

[![Download from the OpenStore](https://open-store.io/badges/en_US.png)](https://open-store.io/app/londontransit.00p513-dev)

## License

Copyright (C) 2023  Amy King

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License version 3, as published by the
Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranties of MERCHANTABILITY, SATISFACTORY
QUALITY, or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <http://www.gnu.org/licenses/>.
